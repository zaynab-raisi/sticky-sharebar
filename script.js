{
    class StickyShareBar {
        constructor(element) {
            this.element = element;
            this.contentTarget = document.getElementsByClassName('js-sticky-sharebar-target');
            this.showClass = 'sticky-sharebar--on-target';
            this.threshold = '50%'; 
            this.initShareBar();
        }

        initShareBar() {
            if(this.contentTarget.length < 1) {
              this.element.addClass( this.showClass);
              return;
            }
            if(intersectionObserverSupported) {
              this.initObserver(); 
            } else {
              this.element.addClass(this.showClass);
            }
        }

        initObserver() {
            const self = this;
            var observer = new IntersectionObserver(
              function(entries, observer) { 
                self.element.classList.toggle( self.showClass, entries[0].isIntersecting);
              }, 
              {rootMargin: "0px 0px -"+this.threshold+" 0px"}
            );
            observer.observe(this.contentTarget[0]);
        }
    }

    const stickyShareBar = document.getElementsByClassName('js-sticky-sharebar'),
      intersectionObserverSupported = ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype);

    new StickyShareBar(stickyShareBar[0]); 
}